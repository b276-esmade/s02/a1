#1

year = int(input("Enter year: "))

if year > 0:
	if year % 4 == 0:
		if year % 100 == 0:
			if year % 400 == 0:
				print(f"{year} is a leap year")
			else:
				print(f"{year} is NOT a leap year")
		else:
			print(f"{year} is a leap year")
	else:
		print(f"{year} is NOT a leap year")
else:
	print("Invalid input. No zero or negative values")




#2

row = int(input("Enter number of row: "))
col = int(input("Enter number of column: "))


for num in range(row):
	for j in range(col):
		print("*", end=" ") #to print the asterisks horizontally
	print()



# Stretch Goal
	# Add a validation for the leap year input:
	# Strings are not allowed for inputs
	# No zero or negative values
